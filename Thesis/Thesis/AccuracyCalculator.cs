﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thesis
{
    public class AccuracyCalculator
    {
        private int K_MAX = 1;

        private string dirModifier = "use_all";
        private CellType? ignoredType;

        private string resDir;
        private ThesisData thd;
        private string[] colorFilterDirs;
        private int parts;
        private List<List<Patient>> dividedData;
        private Dictionary<Tuple<Patient, Patient>, double> distData;

        private List<Tuple<Label, List<Label>>> resList;
        private List<Patient> resPatientList;

        private List<string> best = new List<string>();

        private bool partitioned_voting = false;
        private int addition = 11;
        private string addition_s = "";

        public AccuracyCalculator(ThesisData td, 
            string dir, int p, CellType? ignored = null,
            bool apply_voting = false)
        {
            ignoredType = ignored;
            partitioned_voting = apply_voting;
            thd = td;
            resDir = dir;
            parts = p;
            
            resPatientList = new List<Patient>();

            if (ignored == CellType.Control)
                dirModifier = "ignore_control";
            if (ignored == CellType.BC)
                dirModifier = "ignore_BC";
            if (ignored == CellType.FAM)
                dirModifier = "ignore_FAM";

            GetLabelData();
            
            colorFilterDirs =
                Directory.GetDirectories(resDir + "\\Results");

            foreach (var subdir in colorFilterDirs)
            {
                string fColor = Helper.ExtractLastName(subdir);
                if (!partitioned_voting)
                {
                    GetDistData(fColor);
                    ApplyCrossvalidation(fColor);
                    CalcAccuracy(fColor);
                }
                /*else // TODO: MAKE IS AUTOMATIC FOR BEST PARTS LATER
                {
                    if (
                      fColor.Equals("Green")
                    || fColor.Equals("Gray")

                    || fColor.Equals("pGreen")
                    || fColor.Equals("pGray")

                    || fColor.Equals("yRed")
                    || fColor.Equals("yGreen")
                    || fColor.Equals("yGray")

                    || fColor.Equals("Red")
                    || fColor.Equals("pRed")

                    || fColor.Equals("Blue") 
                    || fColor.Equals("pBlue") 
                    )
                    {
                        //addition_s = "";
                        //addition = 3;

                    }*/
            }

            string path = resDir + "\\Results\\" + 
                dirModifier + "_" + parts +
                "_best" + ".txt";

            File.WriteAllLines(path, best);
            
            CalcMixed();
        }

        private void CalcMixed()
        {
            int tp = 0;
            int tn = 0;
            int p = 0;
            int n = 0;

            foreach (var patient in resPatientList)
            {
                if (patient.ClassifiedType[Label.Positive] +
                    patient.ClassifiedType[Label.Negative] != addition)
                    throw new Exception("unexpected behaviour!");

                Label curr = Label.Negative;

                if (patient.ClassifiedType[Label.Positive] >
                    patient.ClassifiedType[Label.Negative])
                    curr = Label.Positive;

                if (curr == Label.Positive)
                {
                    ++p;

                    if (Helper.GetLabel(patient.RealType) == curr)
                    {
                        ++tp;
                    }
                }

                else
                {
                    ++n;

                    if (Helper.GetLabel(patient.RealType) == curr)
                    {
                        ++tn;
                    }
                }
            }

            int total = p + n;
            double sens = (double)tp / p;
            double spec = (double)tn / n;
            double acc = (sens + spec) / 2;

            string str = $"1 {sens} {spec} {acc}";

            string path = resDir + "\\Results\\" +
                dirModifier + "_" + parts +
                "_mixed_" + addition_s + 
                "_" + addition + ".txt";

            File.WriteAllText(path, str);
        }

        private void CalcAccuracy(string name)
        {
            List<string> strres = new List<string>();

            double max_acc = -1;
            double best_sens = -1;
            double best_spec = -1;
            int best_k = 0;

            for (int k = 1; k <= K_MAX; k += 2)
            {
                int tp = 0;
                int tn = 0;
                int p = 0;
                int n = 0;

                int idx = 0;

                foreach (var el in resList)
                {
                    int equals = 0;

                    for (int i = 0; i <= k; ++i)
                        if (el.Item2[i] == el.Item1)
                            ++equals;

                    Label curr;

                    if (equals > k / 2)
                        curr = el.Item1;
                    else
                    {
                        if (el.Item1 == Label.Positive)
                            curr = Label.Negative;
                        else
                            curr = Label.Positive;
                    }

                    if (curr == Label.Positive)
                    {
                        ++p;

                        int val = resPatientList[idx].ClassifiedType[Label.Positive];
                        resPatientList[idx].ClassifiedType[Label.Positive] = val + 1;

                        if (el.Item1 == Label.Positive)
                            ++tp;
                    } else
                    {
                        ++n;

                        int val = resPatientList[idx].ClassifiedType[Label.Negative];
                        resPatientList[idx].ClassifiedType[Label.Negative] = val + 1;

                        if (el.Item1 == Label.Negative)
                            ++tn;
                    }
                    
                    ++idx;
                }

                int total = p + n;
                double sens = (double)tp / p;
                double spec = (double)tn / n;
                double acc = (sens + spec) / 2;

                if (acc > max_acc)
                {
                    max_acc = acc;
                    best_k = k;
                    best_sens = sens;
                    best_spec = spec;
                }

                strres.Add($"{k} {tp} {tn} {p} {n} {total} {sens} {spec} {acc}");
            }

            var resResultsColorAddDir = resDir + "\\Results\\" +
                        name + "\\" + dirModifier;
            Directory.CreateDirectory(resResultsColorAddDir);

            string path = resResultsColorAddDir + "\\acc_" + parts + 
                        ".txt";

            File.WriteAllLines(path, strres);

            //string data = $"sens: {best_sens} spec: {best_spec} acc: {max_acc} k: {best_k} color: {name}";
            string data = $"{name} {best_k} {best_sens} {best_spec} {max_acc}";

            Console.WriteLine($"p: {parts} " + data);
            best.Add(data);
        }

        private void GetLabelData()
        {
            dividedData = new List<List<Patient>>();

            if (parts == 0)
            {
                foreach (var type in thd.Patients.Keys)
                {
                    if (ignoredType.HasValue && type == ignoredType.Value)
                        continue;

                    foreach (var patient in thd.Patients[type])
                    {
                        dividedData.Add(new List<Patient> { patient });
                    }
                }

                return;
            }

            for (int i = 0; i < parts; ++i)
                dividedData.Add(new List<Patient>());

            foreach (var type in thd.Patients.Keys)
            {
                if (ignoredType.HasValue && type == ignoredType.Value)
                        continue;

                var res = Helper.Divide(thd.Patients[type], parts);

                for (int i = 0; i < res.Count; ++i)
                    dividedData[i].AddRange(res[i]);
            }
        }

        private void GetDistData(string name)
        {
            var fileName = resDir + "\\Results" 
                + "\\" + name + "\\dist.txt";

            distData =
                new Dictionary<Tuple<Patient, Patient>, double>();

            string[] readText = File.ReadAllLines(fileName);

            foreach (var line in readText)
            {
                var parsed = line.Split(' ');
                var p1 = parsed[0].Split('_');
                var p2 = parsed[1].Split('_');

                Patient pat1 = new Patient(int.Parse(p1[1]), Helper.GetCellType(p1[0]));
                Patient pat2 = new Patient(int.Parse(p2[1]), Helper.GetCellType(p2[0]));

                if (ignoredType.HasValue &&
                    (pat1.RealType == ignoredType.Value || pat2.RealType == ignoredType.Value))
                    continue;

                distData.Add(Tuple.Create(pat1, pat2), double.Parse(parsed[2]));
            }
        }

        private void ApplyCrossvalidation(string name)
        {
            resList = new List<Tuple<Label, List<Label>>>();

            List<string> strres = new List<string>();

            for (int i = 0; i < dividedData.Count; ++i)
            {
                var part = dividedData[i];

                foreach (var patient in part)
                {
                    List<Tuple<Patient, double>> distances =
                        new List<Tuple<Patient, double>>();

                    foreach (var elem in distData.Keys)
                    {
                        if (elem.Item1.Equals(patient) &&
                            !part.Exists(p => p.Equals(elem.Item2)))
                        {
                            distances.Add(Tuple.Create(elem.Item2, distData[elem]));
                        }
                        else if (elem.Item2.Equals(patient) &&
                            !part.Exists(p => p.Equals(elem.Item1)))
                        {
                            distances.Add(Tuple.Create(elem.Item1, distData[elem]));
                        }
                    }

                    distances.Sort((y, x) => y.Item2.CompareTo(x.Item2));

                    resList.Add(Tuple.Create(Helper.GetLabel(patient.RealType),
                        MakeLabelList(distances)));

                    if (!resPatientList.Contains(patient))
                    {
                        patient.ClassifiedType[Label.Negative] = 0;
                        patient.ClassifiedType[Label.Positive] = 0;
                        resPatientList.Add(patient);
                    }

                    strres.Add(MakeString(patient, distances));
                }
            }

            var resResultsColorAddDir = resDir + "\\Results" + "\\" +
                        name + "\\" + dirModifier;
            Directory.CreateDirectory(resResultsColorAddDir);

            string filename = resResultsColorAddDir + "\\" + parts + ".txt";

            File.WriteAllLines(filename, strres);
        }

        private List<Label> MakeLabelList(
            List<Tuple<Patient, double>> nearPatients)
        {
            List<Label> res = new List<Label>();

            foreach (var pat in nearPatients)
                res.Add(Helper.GetLabel(pat.Item1.RealType));

            return res;
        }

        private string MakeString
            (Patient p, List<Tuple<Patient, double>> nearPatients)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(p.ToString());
            
            foreach (var pat in nearPatients)
            {
                if (ignoredType.HasValue && 
                    pat.Item1.RealType == ignoredType.Value)
                    continue;

                sb.Append(" ");
                sb.Append(pat.Item1.ToString());
            }
            
            return sb.ToString();
        }

    }
}
