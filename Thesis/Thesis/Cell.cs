﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Thesis
{
    public class Cell
    {
        public Tuple<int, int> Identifier { private set; get; }
        public Dictionary<Tuple<CellColor, CellFilter>, NucleusImg> ColorFilterImg { protected set; get; }
        
        public Cell(string dir, Tuple<int, int> identifier, List<string> txtFilterChannelCells)
        {
            Identifier = identifier;
            ColorFilterImg = new Dictionary<Tuple<CellColor, CellFilter>, NucleusImg>();

            int amount = 0;

            foreach (var name in txtFilterChannelCells)
            {
                var words = Regex.Split(name, @"[^a-zA-Z]+").ToList();
                words.RemoveAt(0);
                words.RemoveAt(words.Count - 1);

                if (words.Count == 2)
                {
                    string t = words[0];
                    words[0] = words[1];
                    words[1] = t;
                }

                CellColor? color = null;
                CellFilter? filter = null;

                switch (words[0])
                {
                    case "Blue":
                        color = CellColor.Blue;
                        break;
                    case "Red":
                        color = CellColor.Red;
                        break;
                    case "Green":
                        color = CellColor.Green;
                        break;
                    case "Gray":
                        color = CellColor.Gray;
                        break;
                }

                if (words.Count == 1)
                {
                    filter = CellFilter.none;
                }
                else
                {
                    switch (words[1])
                    {
                        case "p":
                            filter = CellFilter.purple;
                            break;
                        case "y":
                            filter = CellFilter.yellow;
                            break;
                    }
                }

                if (color.HasValue && filter.HasValue)
                {
                    ColorFilterImg.Add(Tuple.Create(color.Value, filter.Value),
                        new NucleusImg(dir + "\\" + name));
                    ++amount;
                }
            }

            if (amount != 12)
            {
                //Console.Write($"{Identifier.Item1},{Identifier.Item2} ");
            }

        }
        
    }
}
