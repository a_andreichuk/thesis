﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Thesis
{
    public static class ColorExtensions
    {

        public static Color ToGrayscale(this int brightness)
        {
            return Color.FromArgb(brightness, brightness, brightness);
        }

        public static int FromGrayscale(this Color color)
        {
            return (int)Math.Round(color.GetBrightness() * 255.0);
        }
    }
}
