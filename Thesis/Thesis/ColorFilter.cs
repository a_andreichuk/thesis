﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thesis
{
    public enum CellColor
    {
        Red,
        Green,
        Blue,
        Gray
    }

    public enum CellFilter
    {
        none,
        purple,
        yellow
    }
}
