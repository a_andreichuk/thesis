﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thesis
{
    public static class EdgeDetector
    {
        public static void EdgeDetection(ref Bitmap b, float threshold)
        {
            Bitmap bSrc = (Bitmap)b.Clone();

            /*BitmapData bmData = b.LockBits(new Rectangle(0, 0, b.Width, b.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
            BitmapData bmSrc = bSrc.LockBits(new Rectangle(0, 0, bSrc.Width, bSrc.Height), ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);

            int stride = bmData.Stride;

            unsafe
            {
                byte* p = (byte*)(void*)bmData.Scan0;
                byte* pSrc = (byte*)(void*)bmSrc.Scan0;

                int nOffset = stride - b.Width * 3;
                int nWidth = b.Width - 1;
                int nHeight = b.Height - 1;

                for (int y = 0; y < nHeight; ++y)
                {
                    for (int x = 0; x < nWidth; ++x)
                    {
                        //  | p0 |  p1  |
                        //  |    |  p2  |
                        var p0 = ToGray(pSrc);
                        var p1 = ToGray(pSrc + 3);
                        var p2 = ToGray(pSrc + 3 + stride);

                        if (Math.Abs(p1 - p2) + Math.Abs(p1 - p0) > threshold)
                            p[0] = p[1] = p[2] = 0;
                        else
                            p[0] = p[1] = p[2] = 255;

                        p += 3;
                        pSrc += 3;
                    }
                    p += nOffset;
                    pSrc += nOffset;
                }
            }

            b.UnlockBits(bmData);
            bSrc.UnlockBits(bmSrc);
            */

            for (int i = 0; i < b.Width - 1; i++)
            {
                for (int x = 0; x < b.Height - 1; x++)
                {
                    //  | p0 |  p1  |
                    //  |    |  p2  |
                    int p0 = bSrc.GetPixel(i, x).R;
                    int p1 = bSrc.GetPixel(i, x + 1).R;
                    int p2 = bSrc.GetPixel(i + 1, x + 1).R;

                    if (Math.Abs(p1 - p2) + Math.Abs(p1 - p0) > threshold)
                        b.SetPixel(i, x, Color.Black);
                    else
                        b.SetPixel(i, x, Color.White);
                }
            }

        }

        static unsafe float ToGray(byte* bgr)
        {
            return bgr[2] * 0.299f + bgr[1] * 0.587f + bgr[0] * 0.114f;
        }
    }
}
