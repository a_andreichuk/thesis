﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using static Thesis.ExtBitmap;

namespace Thesis
{
    static class Helper
    {
        // cut off the last line (all zeros) 
        // and also the 1st column (just to make a square) => 159
        private static readonly int N = 159;

        public static void WriteToPng(Bitmap image, string fileName)
        {
            image.Save(fileName, System.Drawing.Imaging.ImageFormat.Png);
        }

        public static Bitmap ReadFromFile(string fileName)
        {
            Bitmap image;

            if (fileName.EndsWith(".png"))
            {
                image = (Bitmap)Image.FromFile(fileName);

                return image;
            }
            else if (fileName.EndsWith(".txt"))
            {
                image = new Bitmap(N, N);

                string[] lines = File.ReadAllLines(fileName);

                if (lines.Length != image.Height + 1)
                    throw new Exception("wrong number of rows");

                int x = 0, y = 0;
                int intValue = 0;

                foreach (string line in lines.Take(image.Width))
                {
                    string[] stringValues = line.Split(null);

                    if (stringValues.Length != image.Width + 1)
                        throw new Exception("wrong number of cols");

                    foreach (string stringValue in stringValues.Skip(1))
                    {
                        intValue = (int)double.Parse(stringValue);
                        image.SetPixel(x, y, intValue.ToGrayscale());
                        ++x;
                    }
                    x = 0;
                    ++y;
                }

                return image;
            }

            throw new Exception("wrong file format");
        }

        public static string StringFromColorFilter(CellColor color, CellFilter filter)
        {
            string res = "";

            switch (filter)
            {
                case CellFilter.purple:
                    res += "p";
                    break;
                case CellFilter.yellow:
                    res += "y";
                    break;
            }

            switch (color) {
                case CellColor.Blue:
                    res += "Blue";
                    break;
                case CellColor.Red:
                    res += "Red";
                    break;
                case CellColor.Gray:
                    res += "Gray";
                    break;
                case CellColor.Green:
                    res += "Green";
                    break;
            }

            return res;
        }

        public static string ExtractLastName(string path)
        {
            return path.Substring(path.LastIndexOf("\\") + 1);
        }

        public static T Clone<T>(T source)
        {
            if (!typeof(T).IsSerializable)
            {
                throw new ArgumentException("The type must be serializable.", "source");
            }

            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, source);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }
        

        public static void Shuffle<T>(this IList<T> list)
        {
            Random rng = new Random();

            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static List<List<T>> Divide<T>(List<T> list, int parts)
        {
            //list.Shuffle();

            List<List<T>> res = new List<List<T>>();

            for (int i = 0; i < parts; ++i)
                res.Add(new List<T>());

            for (int i = 0; i < list.Count; ++i)
                res[i % parts].Add(list.ElementAt(i));

            return res;
        }

        public static Label GetLabel(CellType type)
        {
            switch (type)
            {
                case CellType.BC:
                    return Label.Positive;
                case CellType.FAM:
                    return Label.Negative;
                default:
                    return Label.Negative;
            }
        }

        public static CellType GetCellType(string type)
        {
            if (type == CellType.BC.ToString())
                return CellType.BC;
            if (type == CellType.FAM.ToString())
                return CellType.FAM;
            return CellType.Control;
        }

        public static int CalcMargin(BlurType blurType)
        {
            switch (blurType)
            {
                case BlurType.Mean3x3:
                case BlurType.GaussianBlur3x3:
                case BlurType.Median3x3:
                    return 1;

                case BlurType.Mean5x5:
                case BlurType.GaussianBlur5x5:
                case BlurType.Median5x5:
                case BlurType.MotionBlur5x5:
                    return 2;

                case BlurType.Mean7x7:
                case BlurType.Median7x7:
                    return 3;

                case BlurType.Mean9x9:
                case BlurType.Median9x9:
                    return 4;

                case BlurType.Median11x11:
                    return 5;
            }

            throw new Exception("Unsupported blur type");
        }
    }
}
