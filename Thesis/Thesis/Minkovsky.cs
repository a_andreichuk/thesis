﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Thesis
{
    class Minkovsky
    {
        public static IDictionary<double, double> BoxCountingDimension(Bitmap bw, int startSize, int finishSize, int step = 1)
        {
            //length size - number of boxes
            IDictionary<double, double> baList = new Dictionary<double, double>();

            for (int b = startSize; b <= finishSize; b += step)
            {
                int hCount = bw.Height / b;
                int wCount = bw.Width / b;
                bool[,] filledBoxes =
                    new bool[wCount + (bw.Width > wCount * b ? 1 : 0), hCount + (bw.Height > hCount * b ? 1 : 0)];

                for (int x = 0; x < bw.Width; x++)
                {
                    for (int y = 0; y < bw.Height; y++)
                    {
                        if (bw.GetPixel(x, y).R == 0 &&
                            bw.GetPixel(x, y).G == 0 &&
                            bw.GetPixel(x, y).B == 0) //if Black IGNORING ALPHA
                        {
                            int xBox = x / b;
                            int yBox = y / b;
                            filledBoxes[xBox, yBox] = true;
                        }
                    }
                }

                int a = 0;
                for (int i = 0; i < filledBoxes.GetLength(0); i++)
                {
                    for (int j = 0; j < filledBoxes.GetLength(1); j++)
                    {
                        if (filledBoxes[i, j])
                        {
                            a++;
                        }
                    }
                }

                baList.Add(Math.Log(1d / b), Math.Log(a));
            }

            return baList;
        }

        public static double[] NormalEquations2d(double[] y, double[] x)
        {
            //x^t * x
            double[,] xtx = new double[2, 2];
            for (int i = 0; i < x.Length; i++)
            {
                xtx[0, 1] += x[i];
                xtx[0, 0] += x[i] * x[i];
            }
            xtx[1, 0] = xtx[0, 1];
            xtx[1, 1] = x.Length;

            //inverse
            double[,] xtxInv = new double[2, 2];
            double d = 1 / (xtx[0, 0] * xtx[1, 1] - xtx[1, 0] * xtx[0, 1]);
            xtxInv[0, 0] = xtx[1, 1] * d;
            xtxInv[0, 1] = -xtx[0, 1] * d;
            xtxInv[1, 0] = -xtx[1, 0] * d;
            xtxInv[1, 1] = xtx[0, 0] * d;

            //times x^t
            double[,] xtxInvxt = new double[2, x.Length];
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < x.Length; j++)
                {
                    xtxInvxt[i, j] = xtxInv[i, 0] * x[j] + xtxInv[i, 1];
                }
            }

            //times y
            double[] theta = new double[2];
            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < x.Length; j++)
                {
                    theta[i] += xtxInvxt[i, j] * y[j];
                }
            }

            return theta;
        }

        public static double MinkovskyDim(Bitmap image)
        {
            IDictionary<double, double> baList = BoxCountingDimension(image, 1, 10, 1);

            double[] y = new double[baList.Count];
            double[] x = new double[baList.Count];
            int c = 0;

            foreach (double key in baList.Keys)
            {
                y[c] = baList[key];
                x[c] = key;
                c++;
            }

            double[] theta = NormalEquations2d(y, x);

            return theta[0];
        }
    }
}
