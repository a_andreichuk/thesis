﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thesis
{
    public enum NucleusExtractType
    {
        ReadTxt,
        ReadTxtWritePng,
        ReadPng
    }

    public class NucleusHelper
    {
        private NucleusExtractType type;
        private ExtBitmap.BlurType bt;
        private int morphSize;

        public NucleusHelper(NucleusExtractType extractType,
            ExtBitmap.BlurType blurType, int morphOperSize)
        {
            type = extractType;
            bt = blurType;
            morphSize = morphOperSize;
        }

        public NucleusImg GetNucleusImg(NucleusImg img, string filename="")
        {
            if (type == NucleusExtractType.ReadPng)
            {
                img = new NucleusImg(filename + ".png");
                img.Read();
            }
            else
            {
                img.Read();
                img.ApplyBlurring(bt);
                img.Binarize();
                img.ApplyOpenClose(morphSize);
                img.ExtractEdges();
                
                if (type == NucleusExtractType.ReadTxtWritePng)
                {
                    Helper.WriteToPng(img.Image,
                        filename + ".png");
                }
            }

            return img;
        }
    }
}
