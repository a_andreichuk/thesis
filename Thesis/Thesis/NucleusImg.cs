﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using static Thesis.ExtBitmap;

namespace Thesis
{
    public class NucleusImg
    {
        public Bitmap Image { private set; get; }

        private string fileName;

        public NucleusImg (string fileName)
        {
            this.fileName = fileName;
        }

        public void Read()
        {
            Image = Helper.ReadFromFile(fileName);
        }

        public void Delete()
        {
            Image.Dispose();
        }

        public void ApplyBlurring(BlurType blurType)
        {
            Bitmap blurred = Image.ImageBlurFilter(blurType);

            int margin = Helper.CalcMargin(blurType);

            blurred = Helper.Clone(
                blurred.CropBitmap(margin, margin, 
                blurred.Width - margin * 2, blurred.Height - margin * 2));

            Image = blurred;
        }

        public void Binarize()
        {
            Bitmap otsuImage = Helper.Clone(Image);
            Otsu.ApplyOtsuThreshold(ref otsuImage);

            Image = otsuImage;
        }

        public void ApplyOpenClose(int size)
        {
            if (size == 0)
                return;

            var openedImage = Image.OpenMorphologyFilter(size);
            Image = openedImage.CloseMorphologyFilter(size);
        }

        public void ExtractEdges()
        {
            Bitmap image = Image;

            EdgeDetector.EdgeDetection(ref image, 1);
            Image = Helper.Clone(image.CropBitmap(0, 0, 
                image.Width - 1, image.Height - 1));
        }

        public double CalculateFractalDimension(FractalDimType type)
        {
            if (type == FractalDimType.Minkowski)
                return Minkovsky.MinkovskyDim(Image);

            return 0;
        }
        
    }
}
