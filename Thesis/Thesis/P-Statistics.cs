﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thesis
{
    public static class P_Statistics
    {
        private static readonly int g = 3;

        public static double CalculateDistance(double[] vec1, double[] vec2)
        {
            Array.Sort(vec1);
            Array.Sort(vec2);

            return (CalculatePStatistics(vec1, vec2) + 
                CalculatePStatistics(vec2, vec1)) / 2;
        }

        private static double CalculatePStatistics(double[] vec1, double[] vec2)
        {
            int N = vec1.Length * (vec1.Length - 1) / 2;
            int L = 0;

            for (int i = 0; i < vec1.Length - 1; ++i)
            {
                for (int j = i + 1; j < vec1.Length; ++j)
                {
                    double h_ij = 0;

                    for (int k = 0; k < vec2.Length; ++k)
                    {
                        if (vec2[k] > vec1[i] && vec2[k] < vec1[j])
                        {
                            ++h_ij;
                        }
                    }

                    h_ij /= vec2.Length;

                    var p_1_2 = P_1_2(h_ij, vec2.Length);
                    var p_ij = P_ij(i, j, vec1.Length);

                    if (p_ij > p_1_2.Item1 && p_ij < p_1_2.Item2)
                    {
                        ++L;
                    }
                }
            }

            return (double)L / N;
        }

        private static double P_ij(int i, int j, int n)
        {
            return j > i ? ((double)(j - i)) / (n + 1) : 0;
        }

        private static Tuple<double, double> P_1_2(double h_ij, int m)
        {
            double g2 = ((double)g * g);

            double n_1 = h_ij * m + g2 / 2;
            double d = m + g2;
            double n_2 = g * Math.Sqrt(h_ij * (1 - h_ij) * m + g2 / 4);

            double p_1 = (n_1 - n_2) / d;
            double p_2 = (n_1 + n_2) / d;

            return Tuple.Create(p_1, p_2);
        }
    }
}
