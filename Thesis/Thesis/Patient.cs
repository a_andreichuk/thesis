﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Thesis
{
    public class Patient
    {
        public CellType RealType { private set; get; }
        public int PatientNumber { private set; get; }
        public List<Cell> Cells { private set; get; }
        public Dictionary<Label, int> ClassifiedType { set; get; }

        public Patient(int number, CellType type)
        {
            PatientNumber = number;
            RealType = type;

            ClassifiedType = new Dictionary<Label, int>();
            ClassifiedType.Add(Label.Negative, 0);
            ClassifiedType.Add(Label.Positive, 0);
        }

        public Patient(string dir, CellType type, bool readCells = true)
        {
            RealType = type;
            PatientNumber = int.Parse(Helper.ExtractLastName(dir));

            if (!readCells)
                return;

            ClassifiedType = new Dictionary<Label, int>();
            ClassifiedType.Add(Label.Negative, 0);
            ClassifiedType.Add(Label.Positive, 0);

            Cells = new List<Cell>();

            string[] files = Directory.GetFiles(dir);
            List<string> names = files.Select(
                filename => Helper.ExtractLastName(filename)).ToList();
            
            if (names.Count % 12 != 0)
            {
                //Console.WriteLine($"\n Patient #{PatientNumber} has {names.Count} cells. It !%12");
            }

            Dictionary<Tuple<int, int>, List<string>> cellSets = 
                new Dictionary<Tuple<int, int>, List<string>>();

            foreach (string name in names)
            {
                string[] nums = Regex.Split(name, @"\D+"); ;

                var cellIdentifier = Tuple.Create(
                        int.Parse(nums[0]), int.Parse(nums[1]));

                if (!cellSets.ContainsKey(cellIdentifier))
                    cellSets.Add(cellIdentifier, new List<string>());

                cellSets[cellIdentifier].Add(name);
            }

            foreach (Tuple<int, int> cellIdentifier in cellSets.Keys)
            {
                if (cellSets[cellIdentifier].Count % 12 == 0)
                {
                    Cells.Add(new Cell(dir, cellIdentifier, cellSets[cellIdentifier]));
                }
            }
        }

        public bool Equals(Patient other)
        {
            return PatientNumber == other.PatientNumber &&
                RealType == other.RealType;
        }

        public override string ToString()
        {
            return RealType.ToString() + "_" + PatientNumber.ToString();
        }
    }
}
