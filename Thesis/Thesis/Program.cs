﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using static Thesis.ExtBitmap;
using System.IO;
using System.Text.RegularExpressions;

namespace Thesis
{
    class Program
    {
        static void ProcessDir(ThesisData td, string resDir, 
            NucleusHelper nucleusHelper,
            FractalDimType? fracDimType = null,
            string fractalDir = "")
        {
            foreach (var type in td.Patients.Keys)
            {
                Console.WriteLine($"\n{type}");

                foreach (var patient in td.Patients[type])
                {
                    if (type == CellType.BC && patient.PatientNumber < 445)
                        continue;

                    foreach (var cell in patient.Cells)
                    {
                        foreach (var colorFilter in cell.ColorFilterImg.Keys)
                        {
                            /*if (colorFilter.Item1.Equals(CellColor.Blue))
                                continue;
                            if (colorFilter.Item1.Equals(CellColor.Red) &&
                                !colorFilter.Item2.Equals(CellFilter.yellow))
                                continue;
                            if (colorFilter.Item1.Equals(CellColor.Gray) &&
                               colorFilter.Item2.Equals(CellFilter.none))
                                continue;*/

                            /*var resDataDir = resDir + "\\Data";
                            Directory.CreateDirectory(resDataDir);

                            var resDataColorDir = resDataDir + "\\" +
                                Helper.StringFromColorFilter(colorFilter.Item1,
                                                            colorFilter.Item2);
                            Directory.CreateDirectory(resDataColorDir);

                            var fileName = resDataColorDir +
                                "\\" + patient.ToString() + 
                                "_" + cell.Identifier;*/

                            NucleusImg nucleus = 
                                nucleusHelper.GetNucleusImg(
                                    cell.ColorFilterImg[colorFilter] /*, fileName*/);
                            
                            if (fracDimType.HasValue)
                            {
                                var frDataDir = fractalDir + "\\Data";
                                Directory.CreateDirectory(frDataDir);

                                var frDataColorDir = frDataDir + "\\" +
                                    Helper.StringFromColorFilter(colorFilter.Item1,
                                                                colorFilter.Item2);
                                Directory.CreateDirectory(frDataColorDir);

                                var frFileName = frDataColorDir +
                                    "\\" + patient.ToString() +
                                    "_" + cell.Identifier;

                                var minDim = nucleus.CalculateFractalDimension(fracDimType.Value);
                                File.WriteAllText(frFileName + ".txt", minDim.ToString());
                            }
                            
                            nucleus.Delete();

                            //Console.WriteLine($"{patient.PatientNumber}, {cell.Identifier} - done");
                        }
                    }

                    Console.Write($"{patient.PatientNumber} ");
                }
            }
            
        }


        static void CalcAllDistances(ThesisData td, string resDir)
        {
            string[] colorFilterDirs = Directory.GetDirectories(resDir + "\\Data");
            
            foreach (string subdir in colorFilterDirs)
            {
                List<Tuple<CellType, int, double[]>> distData =
                    new List<Tuple<CellType, int, double[]>>();

                Console.WriteLine($"{Helper.ExtractLastName(subdir)}");
                foreach (var type in td.Patients.Keys)
                {
                    Console.WriteLine($"{type}");
                    foreach (var patient in td.Patients[type])
                    {
                        double[] minkArr = new double[patient.Cells.Count];
                        int i = 0;

                        foreach (var cell in patient.Cells)
                        {
                            var cellFileName = subdir + 
                                "\\" + patient.ToString() +
                                "_" + cell.Identifier + ".txt";

                            minkArr[i] = double.Parse(File.ReadAllText(cellFileName));
                            ++i;
                        }

                        distData.Add(
                            Tuple.Create(type, patient.PatientNumber, minkArr)
                            );
                    }
                }

                var resResultsDir = resDir + "\\Results";
                Directory.CreateDirectory(resResultsDir);

                var resResultsColorDir = resResultsDir +
                    "\\" + Helper.ExtractLastName(subdir);
                Directory.CreateDirectory(resResultsColorDir);

                var fileName = resResultsColorDir + "\\" + "dist.txt";
                ProcessDistData(fileName, distData);
            }
        }

        static void ProcessDistData(string file, 
            List<Tuple<CellType, int, double[]>> distData)
        {
            List<string> lines = new List<string>();

            for (int i = 0; i < distData.Count - 1; ++i)
            {
                for (int j = i + 1; j < distData.Count; ++j)
                {
                    double dist = P_Statistics.CalculateDistance(
                        distData.ElementAt(i).Item3, distData.ElementAt(j).Item3
                        );

                    lines.Add($"{distData.ElementAt(i).Item1}_{distData.ElementAt(i).Item2}" +
                        $" {distData.ElementAt(j).Item1}_{distData.ElementAt(j).Item2}" +
                        $" {dist}");
                }
            }

            File.WriteAllLines(file, lines);
        }
        
        static void CalcAccuracy(ThesisData td, string resDir,
            List<int> parts, CellType? ignored = null)
        {
            foreach (var partNum in parts)
            {
                AccuracyCalculator ac =
                    new AccuracyCalculator(td, resDir, partNum,
                    ignored);
            }

        }

        static void TestMain(string filename)
        {
            NucleusImg img = new NucleusImg(filename + ".txt");
            img.Read();

            Helper.WriteToPng(img.Image, filename + "_0.png");

            img.ApplyBlurring(BlurType.Median7x7);

            Helper.WriteToPng(img.Image, filename + "_1.png");

            img.Binarize();

            Helper.WriteToPng(img.Image, filename + "_2.png");

            img.ApplyOpenClose(5);

            Helper.WriteToPng(img.Image, filename + "_3.png");

            img.ExtractEdges();

            Helper.WriteToPng(img.Image, filename + "_4.png");

            img.Delete();
        }

        static void ProcessDataToTxt(ThesisData td, string txtResDir,
                                    BlurType bt, int morphSize)
        {
            foreach (var type in td.Patients.Keys)
            {
                Console.WriteLine($"\n{type}");

                foreach (var patient in td.Patients[type])
                {
                    foreach (var cell in patient.Cells)
                    {
                        foreach (var colorFilter in cell.ColorFilterImg.Keys)
                        {
                            /*if (colorFilter.Item1.Equals(CellColor.Blue))
                                continue;
                            if (colorFilter.Item1.Equals(CellColor.Red) &&
                                !colorFilter.Item2.Equals(CellFilter.yellow))
                                continue;
                            if (colorFilter.Item1.Equals(CellColor.Gray) &&
                               colorFilter.Item2.Equals(CellFilter.none))
                                continue;*/

                            if (colorFilter.Item1.Equals(CellColor.Blue) &&
                                colorFilter.Item2.Equals(CellFilter.yellow))
                                continue;

                            var frDataDir = txtResDir + "\\Txt";
                            Directory.CreateDirectory(frDataDir);

                            var frDataColorDir = frDataDir + "\\" +
                                Helper.StringFromColorFilter(colorFilter.Item1,
                                                            colorFilter.Item2);
                            Directory.CreateDirectory(frDataColorDir);

                            var frFileName = frDataColorDir +
                                "\\" + patient.ToString() +
                                "_" + cell.Identifier;

                            WriteImageInTxt(cell.ColorFilterImg[colorFilter], frFileName + ".txt", bt, morphSize);
                            
                            //Console.WriteLine($"{patient.PatientNumber}, {cell.Identifier} - done");
                        }
                    }
                    Console.Write($"{patient.PatientNumber} ");
                }
            }

        }
        
        static void WriteImageInTxt(NucleusImg img, string filenameTo,
                                    BlurType bt, int morphSize)
        {
            List<string> strres = new List<string>();

            img.Read();

            Bitmap realImg = img.Image;

            img.ApplyBlurring(bt);
            img.Binarize();
            img.ApplyOpenClose(morphSize);

            StringBuilder sb = new StringBuilder("");

            int offs = (realImg.Width - img.Image.Width) / 2;

            for (int col = 0; col < img.Image.Width; col++)
            {
                for (int row = 0; row < img.Image.Height; row++)
                {
                    Color binColor = img.Image.GetPixel(row, col);

                    Color realColor;
                    if (binColor.R == 255)
                        realColor = realImg.GetPixel(row + offs, col + offs);
                    else
                        realColor = Color.Black;

                    sb.Append(string.Format("{0,4} ", realColor.R));
                }

                strres.Add(sb.ToString());
                sb.Clear();
            }

            img.Delete();
            realImg.Dispose();

            File.WriteAllLines(filenameTo, strres);
        }

        static void Unfiltered()
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();

            string sourceDir = "D:\\cancer\\CANCER";
            string resDir = "D:\\cancer\\CANCER\\processed";

            ThesisData td = new ThesisData(sourceDir);

            FractalDimType fracDimType = FractalDimType.Minkowski;
            BlurType blurType = BlurType.Median7x7;
            int morphOperSize = 5;

            resDir += $"\\{blurType}";
            Directory.CreateDirectory(resDir);

            CellType? ignored = null;

            NucleusHelper nucleusHelper =
                new NucleusHelper(NucleusExtractType.ReadTxt,
                blurType, morphOperSize);

            string frDir = resDir + $"\\{fracDimType}";
            Directory.CreateDirectory(resDir);

            //ProcessDir(td, resDir, nucleusHelper, fracDimType, frDir);

            //CalcAllDistances(td, frDir);
            //frDir = "D:\\cancer\\CANCER\\processed\\checker2";

            //0 means that each patient separately
            List<int> parts = new List<int>
                                    { 0 };

            CalcAccuracy(td, frDir, parts);

            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;

            Console.WriteLine();
            Console.WriteLine($"time: {elapsedMs}");
            Console.ReadKey();
        }

        static void GetResults()
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();

            string sourceDir = "D:\\cancer\\CANCER";
            string frDir = "D:\\cancer\\CANCER\\processed\\Median7x7\\Minkowski";

            ThesisData td = new ThesisData(sourceDir);

            //0 means that each patient separately
            List<int> parts = new List<int>
                                    { /*0, 5, 10, 20*/ 0 };

            CalcAccuracy(td, frDir, parts);

            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;

            Console.WriteLine();
            Console.WriteLine($"time: {elapsedMs}");
            Console.ReadKey();
        }

        static void MinMaxCellNum()
        {
            string sourceDir = "D:\\cancer\\CANCER";
            ThesisData td = new ThesisData(sourceDir);

            int minNum = 500;
            int maxNum = 0;

            foreach (var type in td.Patients.Keys)
            {
                foreach (var patient in td.Patients[type])
                {
                    int curr = patient.Cells.Count;

                    if (curr > maxNum)
                        maxNum = curr;
                    if (curr < minNum)
                        minNum = curr;
                }
            }

            Console.WriteLine($"Min: {minNum}, max: {maxNum}");
        }

        static void DifferentBlurring()
        {
            string filename = "D:\\cancer\\test\\nucleus";

            NucleusImg img = new NucleusImg(filename + ".txt");

            List<BlurType> list = new List<BlurType> {
                            BlurType.Mean5x5,
                            BlurType.GaussianBlur5x5,
                            BlurType.MotionBlur5x5,
                            BlurType.Median5x5
                            };

            foreach (var type in list)
            {
                img.Read();

                img.ApplyBlurring(type);

                Helper.WriteToPng(img.Image, filename +
                    "_" + type + ".png");
            }
        }

        static void Main(string[] args)
        {
            try
            {
                FractalDimType fracDimType = FractalDimType.Minkowski;
                BlurType blurType = BlurType.Median7x7;
                int morphOperSize = 5;

                string sourceDir1 = "D:\\cancer\\CANCER";
                ThesisData td1 = new ThesisData(sourceDir1);
                string txtResDir = "D:\\cancer\\CANCER\\processed\\Median7x7_5";

                ProcessDataToTxt(td1, txtResDir, blurType, morphOperSize);
                return;

                //DifferentBlurring();

                //MinMaxCellNum();

                //GetResults();

                //TestMain("D:\\cancer\\test\\nucleus");


                /*var watch = System.Diagnostics.Stopwatch.StartNew();

                string sourceDir = "D:\\cancer\\CANCER";
                string resDir = "D:\\cancer\\CANCER\\processed";
                Directory.CreateDirectory(resDir);

                ThesisData td = new ThesisData(sourceDir);

                resDir += $"\\{blurType}_{morphOperSize}";
                Directory.CreateDirectory(resDir);

                CellType? ignored = null;

                NucleusHelper nucleusHelper =
                    new NucleusHelper(NucleusExtractType.ReadPng,
                    blurType, morphOperSize);

                //ProcessDir(td, resDir, nucleusHelper);

                NucleusHelper nucleusHelper2 =
                    new NucleusHelper(NucleusExtractType.ReadPng,
                    blurType, morphOperSize);

                string frDir = resDir + $"\\{fracDimType}";
                Directory.CreateDirectory(resDir);

                //ProcessDir(td, resDir, nucleusHelper2, fracDimType, frDir);

                //CalcAllDistances(td, frDir);
                //frDir = "D:\\cancer\\CANCER\\processed\\checker2";

                CalcAccuracy(td, frDir, ignored);

                watch.Stop();
                var elapsedMs = watch.ElapsedMilliseconds;

                Console.WriteLine();
                Console.WriteLine($"time: {elapsedMs}");
                Console.ReadKey();*/

            }
            catch (Exception e)
            {
                Console.WriteLine("{0} \n {1}", e.Message, e.StackTrace);
                Console.ReadKey();
            }
        }
    }
}
