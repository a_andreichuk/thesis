﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thesis
{
    public class ThesisData
    {
        public Dictionary<CellType, List<Patient>> Patients { private set; get; }

        public ThesisData(string dir, bool readCells = true)
        {
            string[] typedSetDirs = Directory.GetDirectories(dir);

            Patients = new Dictionary<CellType, List<Patient>>();

            foreach (string typedSetDir in typedSetDirs)
            {
                CellType? type = null;

                switch (Helper.ExtractLastName(typedSetDir))
                {
                    case "BC":
                        type = CellType.BC;
                        break;
                    case "FAM":
                        type = CellType.FAM;
                        break;
                    case "Control":
                        type = CellType.Control;
                        break;
                }

                if (type.HasValue) {
                    var listOfPatients = CreateListOfPatients(typedSetDir, type.Value, readCells);
                    //Console.WriteLine($"{type.Value} : {listOfPatients.Count} patients");
                    Patients.Add(type.Value, listOfPatients);
                }
            }
        }

        private List<Patient> CreateListOfPatients(string dir, CellType type, bool readCells)
        {
            string[] patientDirs = Directory.GetDirectories(dir);
            List<Patient> patients = new List<Patient>();

            foreach (string patientDir in patientDirs)
            {
                patients.Add(new Patient(patientDir, type, readCells));
            }

            return patients;
        }
    }
}
